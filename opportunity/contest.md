# Contest
<p align="center">
    <img  src="../.gitbook/assets/images/contest.PNG">
</p>
Audius is a music streaming platform which is focuses on removing the 3rd party between musicians and their community.
They use their Token to incentivize the operation of nodes and to reward great musicians. Additionally the users can unlock premium features by using the $AUDIO Token.
<br><br>
We want to take another approach. We think that an incentive should be given to content creators and consumers for doing what they love because we love it too.

Aditionally we want to bring some cool features from crypto space right on track to the community and make it as easily accessible to even the newest cryptonian.

<br><br>
See: https://talkaboat.online/pitch