# Market overview
>As we start with Talkaboat, our podcast platform we will focus on the podcast market in this documentation.

Podcasts are becoming more and more popular worldwide and have quickly gained in importance, especially due to COVID-20.
<br><br>
<p align="center">
    <img  src="../.gitbook/assets/images/podcast_average_time.png">
</p>
<br>
The estimated listeners, alone in USA, are expected to reach 117.8 million by end of this year which gives a growth of around 10 % per year. Most of them are between the age of 18 to 34. Also the time they listen to podcasts increases year by year and is estimated to reach 44 minutes per day on 2025.

Not enough numbers?

The revenue for podcasts reached $701 million and was almost doubled in 2021 to $1.33 billion.

### All statistics above are for the US market alone! 

Want to know how strong the US market in podcasting is?

<p align="center">
    <img  src="../.gitbook/assets/images/global-podcast-listening.webp">
    Source: <a href="https://www.podcastinsights.com/de/podcast-statistics/">Podcastinsights</a>
</p>

Sources: https://www.insiderintelligence.com/insights/the-podcast-industry-report-statistics/