# Talkaboat <img src=".gitbook/assets/talkaboat_logo.png" height=30px>
### Viewer and Listeners
 * You love listening to podcasts?
 * You are tired of strange and expensive subscription models? 
 * You want to support your favorite podcaster even if you don't have the money to do so?

### Content Creators
* You are tired to pay for hosting content as you make it for passion and not for money?
* You want to be closer to your community without having to manage 10 different tools?
* You want to earn even with free content?

### WELCOME TO TALKABOAT
Welcome to the new era of podcast.
We know it's hard enough to make awesome content without having to worry about the monetization, marketing and community management - that's why we are here to support you with everything we can. 
#### For free!

* Easy Menu, to access everything fast and smooth
* Cryptocurrency to boost your experience to the next level
* Socialising in every aspect: We even donate and you can decide who will receive the donation

## Earn
You can earn Aboat Tokens simply by listening podcasts. And if that wasn't enough, you can boost your income by doing daily tasks.

Still want more?

Alright, take a look at our [Lounge](core-products/lounge.md) and it's unique elastic reward system.

## Listen
Listen everywhere you are. No matter if you visit [Talkaboat](https://talkaboat.online) in Browser or if you open our App. Create or access your account in a matter of seconds to be ready to earn what you deserve: Tons of Aboat Token!

We aim to become easy accessible, so everyone can find the right podcasts of their interest.

## Create
Your daily work is what we love. As such we want to make it as comfortable as possible for you. If you need features don't be scared to ask for it: [Service @ Talkaboat](mailto:service@talkaboat.online)

You don't have to pay for any service we provide. On the contrary: You earn Aboat Tokens everytime anyone listens to your podcasts. That's not enough? We support you with different monetization methods, like premium content, branding, affiliate or advertising.

## Fees
We are completely free for use. The only fees that occur are with the usage of cryptocurrency. For more information visit the [Fee Section](tokenomics/transfer-fees.md)

## Aboat Token
The engine that keeps us running. Aboat Token will be used in all our future products which gonna be centered around all aspects of media entertainment. At the start there will be 3 ways to get new Aboat Tokens:
- Buying
- Listening
- Yield Farming

In the future we build more ways to earn new Aboat Token like doing tasks, playing games, write comments, rate content creators and of course if someone watches/listens/rates/comments your content. These are just some future ways to earn the aboat token.

## Roadmap
We're developing as fast as possible. But we have to make sure that our code is top-notch and secure. Therefore it might happen that we delay features in order to maintain a high quality.

Q3 2021
- [x] Launch Website
- [x] Release Documentation/Whitepaper
- [x] Release Contracts (Testnet)
- [x] Preview Podcast-Player
- [x] Create Social Media Accounts

Q4 2021
- [x] Create Database
- [x] Release Contract (Mainnet)
- [ ] Incorporation
- [ ] Marketing Campaign
- [ ] Conclude Pre-Sale
- [ ] Profile creation
- [ ] Claim for podcasters
- [ ] Partnerships
- [ ] Improve Podcast-Search
- [ ] Lottery Release

Q1 2022
- [ ] Introduce podcaster-services (hosting)
- [ ] App Preview
- [ ] List on CEX
- [ ] Introduce Development Governance
- [ ] Add more features to Podcast-Player

Q2 2022
- [ ] Release Talkaboat-App
- [ ] Add Community-Groups to Podcast
- [ ] Introduce Community-Battles

Q3 2022
- [ ] Integrate own Wallet to app
- [ ] ---

Q4 2022
- [ ] Preview of Singaboat

2023/2024
- [ ] Gaming Network
- [ ] Game Streaming

