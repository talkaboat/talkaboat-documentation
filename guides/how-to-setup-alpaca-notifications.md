# Alpaca Notifications
### How it works
1. Choose the position you want to recieve notifications on
<img src="../.gitbook/assets/images/alpaca_01.png"  style="margin-top: 10px">
2. Choose your desired Notification Boundary. The Notification Boundary is the limit for your debt ratio. After that enter your e-mail where you want to receive the notifications. Click Update to activate the notification or to update your e-mail/notification boundary.
<img src="../.gitbook/assets/images/alpaca_02.png" style="margin-top: 10px">

3. [Add service@talkaboat.online to your e-mail contacts to prevent the emails going into junk]

<br><br>

After that the checkmark on "Notification Enabled?" should be checked.
<img src="../.gitbook/assets/images/alpaca_03.png" style="margin-top: 10px">

<br>

If you want to remove the notification, simply select the position and click on "Remove".
<img src="../.gitbook/assets/images/alpaca_04.png" style="margin-top: 10px">

<br>

> #### Important notice
> We're currently not storing the notification settings in database. It might happen that we restart the server for maintenance. Check your positions from time to time to make sure it's still enabled. As more Users use this service, we will add a database connection to restore the notification settings after a server restart.