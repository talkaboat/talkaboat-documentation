# <img src="../.gitbook/assets/images/token_logo.png" height="40px">boat Token 
>[Smart Contract](https://github.com/Talkaboat/smart-contracts/blob/master/contracts/AboatToken.sol)
>
>[Blockchain Address] (not released yet)

The total supply will be 1,000,000,000,000 Aboat Token. From this 40 % are not minted as they will be used for yield farming. This 40% will be released into the market step by step over the next ~400 years (see [elastic yield](elastic-yield.md)).

## Specification
- Icon: <img src="../.gitbook/assets/images/token_logo.png" height="18px">
- Name: ABOAT Token
- Symbol: ABOAT
- Network: BSC (we will add more networks in the future)
- Decimals: 18
- Total Supply: 1,000,000,000,000 ABOAT

- Fees: 1% - 5% (see [fees](fees.md)) | Temporary 5% - 10% after launch

## Use Cases
- Yield Farming 
- Rewards
- Pay subscriptions/premium content created by content creators (future)
- Buy merchandise (future)
- Play games (future)

## Anti-Whale 
Our focus is on the community, as such we try to discourage people from obtaining 2 many tokens.

Counter measures we have directly on token:
- Max transaction size (1 % of total supply)
- Increased fee the more tokens someone has

## Anti-Sniper
We have the possibility to increase fee to 90 % before/after we added liquidity.
With this we want to discourage Sniper Bots to buy liquidity right after we added it.

## Additional Measures
We might blacklist people/bots who act suspicious and/or are reported by the community.

>If you happen to be blacklisted and didn't do anything wrong, contact [Service @ Talkaboat](mailto:service@talkaboat.online) and we take a look into it.