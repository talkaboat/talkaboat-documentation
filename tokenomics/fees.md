# Transfer Fees
We have a variable fee structure. The basic fees are 1% of every outgoing transfer.

> Notice: Outgoing transfers are:
> - Sell Talkaboat
> - Remove Liquidity (the fee will be reduced on calling Add Liquidity)
> - Transfer from wallet A to wallet B/exchange


Podcast-Creators can claim their podcast and get whitelisted. This means they are not subject of fees as reciever and all donations send to them will reach them by 100%.
> They will still have fees on sell/sending/liquidity

The higher your total Talkaboat balance is, the more fees will be involved for a transaction.<br>
Start Limit: 5%<br>
Calculation for Fee: Max(5, Min(1, Your total balance/Total Talkaboat supply * 10)<br>
Meaning: If you own atleast 0.5% of the total supply you will have a 5% fee on every transfer

> Example:<br>
> Talkaboat will have a future total supply of 1,000,000,000,000 token.<br>
> If you own 25,000,000,000 and make an outgoing transfer a fee of 2.5% will occur.
<br><br>
<p align="center">
    <img  src="../.gitbook/assets/images/fees.PNG">
</p>
<br>

> - Donation will be distributed to a foundation via community voting. Frequency will be dependent on the fill up speed
> - Liquidity will sell 50% for BNB/BUSD/USDT, add those with Talkaboat as liquidity and burn the liquidity token
> - Redistribution will go back into reward pool
> - Team fee is to keep the whole thing running and will be used for marketing, development, salary and bills.

# Strategic Notice
We start with a temporary fee of 5 - 10 % on outgoing transactions. This helps us to prevent pump and dump and makes it more unlikely for bots to make unbeneficial trades to dump on someone who's buying.
