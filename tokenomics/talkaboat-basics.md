# Talkaboat 1x1
## Tokenomics
We have a total supply of 1,000,000,000,000 Aboat Tokens which will be distributed as shown below.
<p align="center">
    <img src="../.gitbook/assets/images/tokenomics_distribution.PNG">
</p>

The income of the single stages will be distributed by following scheme:

| Sale Type | Limit   | Price per coin | Hardcap | Date | Time 
|---|---|---|---|---|---|
| Private Sale | 0.1 BNB/50BNB | 0,00000002 BNB | 2000 BNB | TBA | TBA
| Lucky Sale | 0.1 BNB/5BNB | 0,00000002 BNB | 200 BNB | TBA | TBA
| Public Sale | 0.1 BNB/20BNB | 0,000000025 BNB | 2250 BNB | TBA | TBA

| Sale Type | Liquidity   | Marketing | Audit | Development | 
|-----|-----|-----|-----|-----|
| Private Sale | 56.25 % | 16.875 % | 10 % | 16.875 % |
| Lucky Sale | 100 % | - | - | - |
| Public Sale | 80 % | 10 % | - | 10 % |

- Release Price: 0,000000025 BNB

Vesting:
- There will be no vesting of Dev Token, as we don't recieve any.
- Marketing Tokens will be vested over 2 years with 10 % instant release (5% per month)
- Private Sale vesting: 20 % instant release, 18 month vesting

>Notice: The sales will take place on https://talkaboat.online/presale    

>Notice: Unsold coins will be sent into reward system

>Notice: Claimables are distributed for listening to both - the creator and the listener

## Contracts
Contracts are currently developed and subject of change.
We did a first audit on https://github.com/Rugfreecoins/Smart-Contract-Audits/blob/main/Talkaboat%20Token%20Audit.pdf

Due to a strategic change we had to do minor adjustments on the contracts.
