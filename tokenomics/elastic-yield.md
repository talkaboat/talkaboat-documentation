# Elastic Yield
We provide you with an unique yield farming experience in our lounge.

Example:<br>
We start with a reward per block of 4000.
Estimating that the binance smart chain verifies 28,800 blocks per day, we distribute 115,200,000 Aboat Tokens per day.

From our total supply 50% is initially reserved for yield farming which equals 500,000,000,000  Talkaboat. So to reach our total supply we need 4,340 days or 12 years.

Quite a long time, huh?<br>
We thought so too, so we defined an elastic yield feature. If the average price of the last 24h raises, the reward per block raises too. If it goes down, the reward per block goes down too.

At first we will set the maximum raise and drop to 2.5% per day.

> Feature is work in progress and might change