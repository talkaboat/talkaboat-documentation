The usage of virtual currencies like ABOAT should always be considered high risk and we want to ensure that everyone who gets in contact with virtual currencies will be aware of that. We won't give any financial advise on whether to buy or sell anything neither on how to use them. As such we're not eligible for any financial losses that ocur by using our system except for system related losses. 

There is always a risk of permanent and impermanent loss when moving in the decentralized finance space of virtual currencies.

We have no influence on the market. As such it might happen that the price is target of high volatility. Even though we try to make any step in the process as transparent as possible it might happen that something is unclear. In that case we would love to get feedback to make our processes open for anyone.

Anonymity is an ideal cover for criminal money, as such we'll use KYC to ensure safety for our customers.