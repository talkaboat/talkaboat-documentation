# Swap
We provide you with a swap service, which is connected to 1inch to give you the best possible rates.

It's important to notice, that some coins (like Talkaboat) come with a transfer fee. Swapping coins comes with a slippage. A trade starts with the transfer and the price may go up or down during the transfer, which might result in less coins for you than you expected. Here comes the slippage into the game. For example you set the slippage to 1%. Now the price may vary up to 1% before the trade gets canceled.

We set a default slippage of 1% which you can change depending on your needs.
For Talkaboat trades we have a function which calculates your minimum required Slippage and adds 1% as the transfer fee is depending on your overall balance of Aboat Tokens.

With 1Inch we only support high liquidity dex like WaultSwap, PancakeSwap, BakerySwap or ApeSwap.


>#### Want to know more about our transfer fees?
>
>Visit our [Fees Page](../tokenomics/fees.md)

<hr>

>#### Don't know how to swap?
>
>Check our guide: [How to trade on Talkaboat](../guides/how-to-trade-on-talkaboat.md)