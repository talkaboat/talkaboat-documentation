# Reward System
The reward system is a Smart Contract with Oracle Service, in which the tokens are locked, which are intended as a reward for the listener and the creator. The smart contract is stored in the Aboat token itself as an address in order to forward the fees from sales, outgoing transactions and the creation of liquidity directly to them.

The amount of token rewarded for listening are dependend on the amount of listeners and are subject of change over time!

## Technical overview
<p align="center">
    <img  src="../.gitbook/assets/images/reward_system.png">
</p>