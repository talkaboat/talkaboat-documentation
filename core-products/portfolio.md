# Portfolio

# Portfolio Tracking

Our Portfolio Tracker shall help you to keep an eye on your coins no matter where they are.
It's a minor priority, so we might add more features after our core features (Token, Podcast, Streaming) are finished and we have some air to breath. 

If you need more features for our portfolio tracker, leave us a mail on [service@talkaboat.online](mailto:service@talkaboat.online)

Thank you for your patience and your support.
## Alpaca Notifications
Alpaca Notifications is our first feature for the portfolio tracker. It allows you to place notifications on your alpaca leveraged farm positions.

Most leveraged farms get liquidated at ~ 80% debt ratio. If you don't want to check it 24/7 you can use our service and set a notification boundary. In case the debt ratio passes the notification boundary you get an information and can increase your collateral or close the position before it gets liquidated.

If you want to learn how to do it, check our guide: [How to setup Alpaca notifications](../guides/how-to-setup-alpaca-notifications.md)
