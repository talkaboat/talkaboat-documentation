# Liquidity
We won't provide an decentralised exchange on our own. We are looking for a strong partnership like WaultSwap, PancakeSwap, ApeSwap or BakerySwap to build on. It's important for us to work with an decentralised exchange where we get to know the team behind and with a good communication.

<!-- We use ApeSwap as the decentralised exchange (dex). So all Liquidity you provide on Talkaboat will be ApeSwap LP and can't be exchanged on other dex like PancakeSwap. We have build a bridge to add and remove liquidity right on talkaboat.online. If the service is not reachable, you can do it on [ApeSwap Liquidity](https://app.apeswap.finance/pool).

The liquidity provider contract is: [0xcF0feBd3f17CEf5b47b0cD257aCf6025c5BFf3b7](https://bscscan.com/address/0xcF0feBd3f17CEf5b47b0cD257aCf6025c5BFf3b7#code) -->

> Our core Liquidity pairs are: ABOAT-BNB, ABOAT-USDT

Want to know how to add and remove liquidity?

Check our guide: [How to provide and remove liquidity](../guides/how-to-provide-and-remove-liquidity.md)

