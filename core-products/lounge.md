# Lounge
As our shipyard is fully automated, we built a little lounge for the engineers. Here you can chill and check your otherworldly stuff while your ordered Talkaboat get produced.

All you have to do is provide a little liquidity or to stake some coins and our machines keep running to provide you with those awesome Aboat Token.

Not sure how to do this?

Check our guide: [How to yield farm on talkaboat](../guides/how-to-yield-farm-on-talkaboat.md)