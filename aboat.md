# Aboat Concept

With Aboat we aim to build an online entertainment ecosystem with 2 core principles:
- Incentivisation
- Gamification

### Incentivisation
We offer different aspects of incentivisation which will be cornered around the different products of podcast, music, video, gaming and many more. The biggest one is our <b>consume and share 2 earn</b>. We reward users for interacting with the content of content creators and at the same time we reward the content creators when someone interacts with their content. The interaction might be listening, watching, playing and more.

### Gamification
Also one of our core principles is gamification which is deeply connected to incentivisation on our platform. With this we aim to make the usage of our products as fun and engaging as possible and also to make it possible for content creators to engage with their community on new and fun ways. One example are our daily tasks. They require the user to do tasks they might have not done normally to engage with our features. We also deliver the possibility for content creators and communities to deploy tasks which will be partly incentivised by us (we reward aboat token for doing tasks) and can be additionally incentivised by the creators.

### One thing fuels them all: Aboat Token
All products will be based on the Aboat Token. From buying premium content, nfts, subscriptions, merch and everything else.

More details on the Aboat Token itself can be found [here](./tokenomics/aboat-token.md)

### Mix of decentralization and centralization
Our platforms are semi decentralized as we also deliver our services for non crypto users. Additionally we took part of the decentralization out of the game to make it possible to make donations for content creators as smooth, fast and easy as possible. As such we have a deep integration of decentralised crypto features into our centralized infrastructure. Rewards for example will be distributed on our central infrastructure and can be claimed on the decentral blockchain thanks to our oracle service which can interact with our reward system smart contract.